# BACK API "RIPLEY"

## Intro 
Este proyecto fue creado para una prueba para el banco ripley

## Directories
```
├── public/      (static files)
│   ├── favicon.ico  
│   ├── index.html (html temlpate)   
│   ├── manifest.json  
│   └── robots.txt
│     
│     
├── src/      
│   ├── Components/ 
│   │   ├── Rut/ 
│   │   └── rut.js
│   │
├   ├── Redux/      
│   │   └── store.js/ 
│   │  
│   ├── Vistas/      
│   │   ├── Carro_ventas /
│   │   │ └── Modal/ 
│   │   │    └── modal.js   
│   │   ├── carro_ventas.js 
│   │   └── style.css    
│   │   │
│   │   ├── Formulario_ventas/  
|   │   │   ├── formulario_venta.js  
│   │   │   └── style.js  
│   │   │ 
│   │   ├── Home/  
│   │   │   ├── Home.js  
│   │   │   └── style.js  
│   │   │ 
│   │   ├── menu/  
│   │   │   ├── menu.js  
|   │   │   ├── routes.js  
│   │   │   └── style.js  
│   │   │
│   │   ├── Mis_Compras/  
|   │   ├── compras.js  
│   │   └── style.js  
│   │   │  
│   ├── Productos/  
|   │   ├── productos.js  
│   │   └── style.js           
│   │  
│   │  
│   ├── app.css     
│   ├──app.js     
│   ├──App.test     
│   ├──index.css     
│   ├──index.js     
│   ├──serviceWorker.js     
│   └──setupTests.js.js     
│   
├── .env           
├── .gitIgnore           
├── package.json           
├── package.lock.json           
├── README.md           
└── yarn.lock
```

#### Para iniciar el proyecto 
- Debemos tener iniciado el XAMP 
- ingresar a la carpeta `cd proyecto-ripley` 
- Instalar dependencias con `npm i` o `npm install`
- iniciar proyecto

## Sctipts para inciar
- `npm start`   
- `npm run dev` 
