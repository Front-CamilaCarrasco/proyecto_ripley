import {createStore} from 'redux';

const initialstate = {
    clienteInfo: {},
    infoProductos: [],
    prod_carrito: [],
    modalVisible: false,
    cantidad_carrito: 0,
    isComprar: false,
    isFormVenta: true,
    ordenVisible: false,
    compraFinalizada: false,
    formularioOrdenCompra: false
};

const reducer = (state = initialstate, action) => {
    if (action.type === "INFO_CLIENTE") {
        return {
            ...state,
            clienteInfo: action.clienteInfo
        };
    }
    if (action.type === "INFO_PRODUCTOS") {
        return {
            ...state,
            infoProductos: action.infoProductos
        };
    }
    if (action.type === "PROD_CARRITO") {
        console.log("se agrego", action.prod_carrito);
        console.log(".prod_carrito", state.prod_carrito);

        return {
            ...state,
            prod_carrito: state.prod_carrito.concat(action.prod_carrito),
        };

    }
    if (action.type === "BORRAR_PRODUCTO_CARRITO") {
        return {
            ...state,
            prod_carrito: state.prod_carrito.filter((element, index) => index !== action.element),

        };
    }
    if (action.type === "LIMPIAR_CARRITO") {
        return {
            ...state,
            prod_carrito: [],

        };
    }
    if (action.type === "MODAL_ISVISIBLE") {
        return {
            ...state,
            modalVisible: action.modalVisible,

        };
    }
    if (action.type === "CANTIDAD_COMPRA") {
        return {
            ...state,
            cantidad_carrito: action.cantidad_carrito,

        };
    }
    if (action.type === "COMPRAR_ISVISIBLE") {
        return {
            ...state,
            isComprar: action.isComprar,

        };
    }
    if (action.type === "VENTA_ISVISIBLE") {
        return {
            ...state,
            isFormVenta: action.isFormVenta,

        };
    }
    if (action.type === "ORDEN_ISVISIBLE") {
        return {
            ...state,
            ordenVisible: action.ordenVisible,

        };
    }
    if (action.type === "COMPRA_FINALIZADA") {
        return {
            ...state,
            compraFinalizada: action.compraFinalizada,
        };
    }
    if (action.type === "FORMULARIO_VENTA") {
        return {
            ...state,
            formularioOrdenCompra: action.formularioOrdenCompra,
        };
    }


    console.log(action);
    return state;

};


export default createStore(reducer);
