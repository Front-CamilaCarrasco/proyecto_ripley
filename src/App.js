import React from "react";
import {HashRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './Redux/store';
import Menu from './Vistas/menu/menu';
import './App.css';


export default function SidebarExample() {
    return (
        <div>
            <Provider store={store}>
                <HashRouter>
                    <div className="nVar-left"
                         style={{
                             display: 'flex',
                             justifyContent: 'center',
                             textAlign: '-webkit-center',
                             width: "100%",
                         }}>

                        <div className="cont-img-left">
                            <div className="img-left">
                                <img
                                    src="https://minisitios.ripley.cl/minisitios/banner-header/portal-banco/paga-tarjeta-20200715-desk.svg"/>
                            </div>
                        </div>
                        <div className="cont-img-rigth">
                            <div className="img-rigth">
                                <img
                                    src="https://minisitios.ripley.cl/minisitios/banner-header/portal-banco/avance-20200713-desk.svg"/>
                            </div>
                        </div>

                    </div>
                    <Menu/>

                </HashRouter>
            </Provider>
        </div>
    );
}
