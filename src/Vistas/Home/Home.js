import React, {Component} from 'react';
import {Card, Col, UncontrolledCarousel} from 'reactstrap';

class Home extends Component {

    render() {
        const items = [
            {
                src: 'https://minisitios.ripley.cl/minisitios/home/generico/slider/sl_colch_202008014_desk.jpg',
                altText: '',
                caption: ''
            },
            {
                src: 'https://minisitios.ripley.cl/minisitios/home/slider/banco-20200805-desk.jpg',
                altText: '',
                caption: ''
            }
        ];
        return (
            <div className="contenedor">
                <div style={{maxHeight: '200px'}}>
                    <div style={{marginBottom: '5px'}}>
                        <UncontrolledCarousel items={items}/>
                    </div>
                    <div style={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        marginBottom: '5px'
                    }}>
                        <Col md={4}>
                            <Card style={{marginTop: '2px'}}>
                                <img width="100%"
                                     src="https://minisitios.ripley.cl/minisitios/home/generico/box/bx14-bs-cocina-20200814-desk.jpg"
                                     alt="Card image cap"/>
                            </Card>
                        </Col>
                        <Col md={4}>
                            <Card style={{marginTop: '2px'}}>
                                <img width="100%"
                                     src="https://minisitios.ripley.cl/minisitios/home/generico/box/bx15-rodados-20200814-desk.jpg"
                                     alt="Card image cap"/>
                            </Card>
                        </Col>
                        <Col md={4}>
                            <Card style={{marginTop: '2px'}}>
                                <img width="100%"
                                     src="https://minisitios.ripley.cl/minisitios/home/generico/box/bx14-bs-cocina-20200814-desk.jpg"
                                     alt="Card image cap"/>
                            </Card>
                        </Col>
                        <Col md={12}>
                            <Card style={{marginTop: '2px'}}>
                                <img width="100%"
                                     src="https://minisitios.ripley.cl/minisitios/home/generico/box/bx17-fotografia-20200814-desk.jpg"
                                     alt="Card image cap"/>
                            </Card>
                        </Col>
                    </div>
                </div>

            </div>

        );
    }
}

export default Home;
