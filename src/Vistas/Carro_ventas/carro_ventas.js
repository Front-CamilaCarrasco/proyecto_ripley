import React, {Component} from 'react';
import './style.css';
import {Button, Col, Row} from 'reactstrap';
import 'antd/dist/antd.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faShoppingBag, faTrashAlt} from '@fortawesome/free-solid-svg-icons';
import connect from "react-redux/es/connect/connect";
import FormularioVenta from "../Formulario_ventas/formulario_venta";

const bolsa = <FontAwesomeIcon style={{color: '#70578b'}} icon={faShoppingBag}/>;
const basurero = <FontAwesomeIcon style={{color: '#a4a4a4', fontSize: '20px'}} icon={faTrashAlt}/>;

class CarroVentas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            totalCompra: 0,
        };
    }

    handleBorrar = (element) => {
        let {handleBorrarProd, cantidad_carrito, handleCantidadProductos} = this.props;
        handleBorrarProd(element);

        let cantidad_productos = cantidad_carrito - 1;

        handleCantidadProductos(cantidad_productos);
    };

    handleCompraCarrito = () => {
        let {prod_carrito, handleComprar} = this.props;
        let totalProducto = 0;
        handleComprar(true);
        prod_carrito.map(element => {
            totalProducto = totalProducto + parseInt(element.Prod_Precio);
            // // console.log(totalProducto);
            this.setState({totalCompra: totalProducto});
        });
    };

    render() {

        let {prod_carrito, isComprar, handleBorrarProd, handleModal} = this.props;
        return (
            <div>

                <div>
                    <div className="contenedor-carrito">
                        {isComprar ?
                            <FormularioVenta totalCompra={this.state.totalCompra}/>
                            : // NO HAY PRODUCTOS EN LA BOLSA
                            <div className="contenedor-carrito-1">
                                {prod_carrito.length === 0 ?

                                    <div className="prod-carrito-vacio">
                                        <div className="titulo-carrito">
                                            <h5> {bolsa} Bolsa de compra </h5>
                                        </div>
                                        <div style={{padding: '170px'}}>
                                            <h3 style={{color: '#4e2e7d'}}>No hay productos en la bolsa</h3>
                                            <div>
                                                <Button size="sm" onClick={() => handleModal(false)}>Cerrar </Button>
                                            </div>
                                        </div>
                                    </div>
                                    // FIN  NO HAY PRODUCTOS EN LA BOLSA
                                    : <div className="prod-carrito">
                                        <div className="titulo-carrito">
                                            <h5> {bolsa} Bolsa de compra </h5>
                                        </div>
                                        <div id="overflow-carrito">
                                            {prod_carrito.map((element, index) =>
                                                    <div style={{
                                                        margin: '10px',
                                                        display: 'flex',
                                                        flexWrap: 'wrap',
                                                        marginleft: '-15px',
                                                    }} key={index}>
                                                        <Col md={3} style={{maxWidth: '180px'}}>
                                                            <img width="100%" src={element.Prod_img}/>
                                                        </Col>
                                                        <Col md={6}>
                                                                <span className="style-marca"
                                                                      id="presentacion_marca_producto">{element.Prod_marca}</span><br></br>
                                                            <span className="style-nombre"
                                                                  id="presentacion_producto">{element.Prod_nombre}</span><br></br>
                                                            <span className="style-nombre"
                                                                  id="presentacion_codigo_producto">Codigo: {element.Prod_codigo}</span>
                                                            <span id="presentacion_info_producto">
                                                                    <span
                                                                        style={{fontSize: '13px'}}> Precio Unidad ${Intl.NumberFormat().format((element.Prod_Precio))} </span>
                                            </span>
                                                        </Col>
                                                        <Col md={3} className="total-center">
                                                            <div
                                                                onClick={() => this.handleBorrar(index)
                                                                }>{basurero}</div>
                                                        </Col>
                                                        <hr id="hr-style-producto-lista"></hr>
                                                    </div>
                                            )}
                                        </div>
                                        <div style={{margin: '10px'}}>
                                            <div style={{
                                                textAlign: 'center',
                                                marginLeft: '200px',
                                                marginRight: '200px'
                                            }}>
                                                <Row>
                                                    <Col md={6}>
                                                        <Button size="sm" className="button_carrito"
                                                                onClick={() => this.handleCompraCarrito()}>Comprar</Button>
                                                    </Col>
                                                    <Col md={6}>
                                                        <Button size="sm"
                                                            className="button_carrito"
                                                                onClick={() => handleModal(false)}>Seguir
                                                            comprando</Button>
                                                    </Col>
                                                </Row>
                                            </div>

                                        </div>
                                    </div>
                                }

                            </div>
                        }
                    </div>
                </div>


            </div>
        );
    }
}

const initMapStateToProps = state => ({
        prod_carrito: state.prod_carrito,
        isComprar: state.isComprar,
        isFormVenta: state.isFormVenta,
        cantidad_carrito: state.cantidad_carrito
    })
;
const initMapDispatchToProps = dispatch => ({
        handleBorrarProd(element) {
            dispatch({
                type: "BORRAR_PRODUCTO_CARRITO",
                element
            });
            // // console.log(element);
        },
        handleModal(modalVisible) {
            dispatch({
                type: "MODAL_ISVISIBLE",
                modalVisible
            });
            // console.log(modalVisible);
        },
        handleComprar(isComprar) {
            dispatch({
                type: "COMPRAR_ISVISIBLE",
                isComprar
            });
            // // console.log(isComprar);
        },
        handleFormularioVenta(isFormVenta) {
            dispatch({
                type: "VENTA_ISVISIBLE",
                isFormVenta
            });
            // // console.log(isFormVenta);
        },
        handleCantidadProductos(cantidad_carrito) {
            dispatch({
                type: "CANTIDAD_COMPRA",
                cantidad_carrito
            });
            // // console.log(cantidad_carrito);
        }

    })

;

export default connect(initMapStateToProps, initMapDispatchToProps)(CarroVentas);

