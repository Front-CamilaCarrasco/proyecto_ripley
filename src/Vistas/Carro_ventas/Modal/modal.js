import React, {Component} from 'react';
import CarroVentas from "../carro_ventas";
import {Badge,  Modal} from 'reactstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import connect from "react-redux/es/connect/connect";


const carrito = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faShoppingCart}/>;


class Modals extends Component {


    render() {
        let {handleModal, modalVisible, cantidad_carrito} = this.props;

        return (
            <>
                {console.log(cantidad_carrito)}
                <div style={{color: '#ffff', marginRight: '10px', fontSize: '20px', cursor: 'pointer'}}
                        onClick={() => handleModal(true)}
                >
                    {carrito} <Badge color={ cantidad_carrito > 0 ?'danger':'secondary'}>  {cantidad_carrito}</Badge>
                </div>
                {
                    modalVisible && <Modal isOpen={modalVisible}>
                        <CarroVentas  />
                    </Modal>
                }

            </>
        );
    }
}

const initMapStateToProps = state => ({
    modalVisible: state.modalVisible,
    cantidad_carrito: state.cantidad_carrito
    })

;
const initMapDispatchToProps = dispatch => ({
        handleModal(modalVisible) {
            dispatch({
                type: "MODAL_ISVISIBLE",
                modalVisible
            });
            console.log(modalVisible);
        },
        handleCantidadProductos(cantidad_carrito) {
        dispatch({
            type: "CANTIDAD_COMPRA",
            cantidad_carrito
        });
        console.log(cantidad_carrito);
    }
    })
;

export default connect(initMapStateToProps, initMapDispatchToProps)(Modals);



