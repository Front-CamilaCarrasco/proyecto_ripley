import React, {Component} from 'react';
import axios from "axios";
import {connect} from 'react-redux';
import {Button, Card, CardBody, CardSubtitle, CardTitle, Col, Input, Label, Row} from 'reactstrap';
import Swal from 'sweetalert2';
import './style.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faStar, faUndoAlt} from '@fortawesome/free-solid-svg-icons';

const atras = <FontAwesomeIcon icon={faUndoAlt}/>;
const estrella = <FontAwesomeIcon style={{color: '#ffc300'}} icon={faStar}/>;

class Productos extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cantidad_carrito: 0,
            prod_total: 0,
            text: '',
            producto: [],
            cliente: {},
            mostrarProducto: true,
        };
    }


    handleMostrarProducto = (element) => {
        // console.log(element);
        let {handleInfoProducto, infoProductos} = this.props;
        let {mostrarProducto} = this.state;
        handleInfoProducto(element);
        this.setState({mostrarProducto: !mostrarProducto});
    };

    componentDidMount() {
        this.getProducts();
        let {infoOrden, compraFinalizada, modalVisible, formularioOrdenCompra, isFormVenta, isComprar} = this.props;
        console.log(
            infoOrden, compraFinalizada, modalVisible, formularioOrdenCompra, isFormVenta, isComprar
        );
    }

    handleAgregarProd = (element) => {
        let {cantidad_carrito} = this.state;
        let {handleProdBolsa, handleCantidadProductos, cant_compra} = this.props;
        handleProdBolsa(element);
        cantidad_carrito = cantidad_carrito + 1;
        this.setState({cantidad_carrito: cantidad_carrito});

        handleCantidadProductos(cantidad_carrito);
        console.log("aqui esta la cantidad");
        console.log(cantidad_carrito);
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer);
                toast.addEventListener('mouseleave', Swal.resumeTimer);
            }
        });

        Toast.fire({
            icon: 'success',
            title: 'Se agrego al carrito'
        });
    };


    getProducts = () => {
        let array = [];
        let contador = 0;
        axios.get(`${process.env.REACT_APP_API_URL}getProductos`)
            .then(res => {
                array = res.data.result;
                array.map((element) => {
                    contador = contador + 1;
                });
            }).then(() => {
            this.setState({
                producto: array,
                prod_total: contador
            });
        });
    };

    render() {
        let {infoProductos} = this.props;
        let {prod_total, producto, mostrarProducto} = this.state;

        return (
            <div className="contenedor">
                <div className="contenedor-lista-productos">
                    <div className="titulo">
                        <h3>Perfumería</h3>
                    </div>
                    <div className="subTitulo">
                        <Row>
                            <Col md={8}>
                                <div className="subTitulo-style">
                                    <h5> {`${prod_total} Resultados en Perfumería`}  </h5>
                                </div>
                            </Col>
                            {mostrarProducto ? '' : <Col md={4}>
                                <div style={{float: 'right'}}>
                                    <Button className="button-atras"
                                            onClick={() => this.handleMostrarProducto()}> {atras} Atras</Button>
                                </div>
                            </Col>}
                        </Row>
                    </div>
                    <div style={{minHeight: '2px', background: 'rgb(165 165 165)', width: '100%'}}></div>

                    <div className="productos">
                        <Col md={3}>
                            <div className="filtro">
                                <span>Filtro</span>
                            </div>
                            <div className="style-filtro-contenedor">
                                <div className="span-title">
                                    Marca
                                </div>
                                <div>
                                    {producto.map((element, index) =>
                                        // console.log(element)
                                        <div className="small-filtro">
                                            <Label check>
                                                <Input type="checkbox"/>{' '}
                                                {element.Prod_marca}
                                            </Label>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="style-filtro-contenedor">
                                <div className="span-title">
                                    Precio
                                </div>
                                <div className="small-filtro">
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Desde $10.990 hasta $30.990
                                    </Label>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Desde $30.990 hasta $60.990
                                    </Label>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Desde $60.990 hasta $90.990
                                    </Label>
                                    <Label check>
                                        <Input type="checkbox"/>{' '}
                                        Desde $90.990 hasta $100.990
                                    </Label>
                                </div>
                            </div>

                        </Col>
                        <Col md={9}>
                            {mostrarProducto ? <Row>
                                    {producto.map((element, index) =>
                                        <Col md={4} onClick={() => this.handleMostrarProducto(element)}>
                                            <Card style={{cursor: 'pointer'}}>
                                                <img width="100%" src={element.Prod_img} alt="imagen"/>
                                                <CardBody>
                                                    <span className="style-marca">{element.Prod_marca}</span>
                                                    <CardTitle className="style-nombre">{element.Prod_nombre}</CardTitle>
                                                    <CardSubtitle
                                                        className="precio">${Intl.NumberFormat().format(element.Prod_Precio + 10000)}</CardSubtitle>
                                                    <CardSubtitle
                                                        className="precio-oferta">${Intl.NumberFormat().format((element.Prod_Precio))}</CardSubtitle>

                                                </CardBody>
                                            </Card>
                                        </Col>
                                    )}
                                </Row> :
                                <div>
                                    <Row>
                                        <Col md={6}>
                                            <img width="100%" src={infoProductos.Prod_img}/>
                                        </Col>
                                        <Col md={6}>
                                            <span className="style-marca"
                                                  id="presentacion_marca_producto">{infoProductos.Prod_marca}</span><br></br>
                                            <span className="style-nombre"
                                                  id="presentacion_producto">{infoProductos.Prod_nombre}</span><br></br>
                                            <span className="style-nombre"
                                                  id="presentacion_codigo_producto">Codigo: {infoProductos.Prod_codigo}</span>

                                            <div>
                                                {estrella}{estrella}{estrella}{estrella}{estrella} (5 Calificaciones)
                                            </div>

                                            <div style={{marginTop: '30px'}}>
                                                <Col md={12}>
                                                    <Row>
                                                        <Col md={9} id="presentacion_info_producto">
                                                            <div> Precio Normal</div>
                                                        </Col>
                                                        <Col md={3} id="presentacion_info_producto"
                                                             style={{textDecoration: 'line-through'}}>${Intl.NumberFormat().format((infoProductos.Prod_Precio + 10000))} </Col>
                                                        <Col md={9} id="presentacion_info_producto">
                                                            <div> Precio Oferta</div>
                                                        </Col>
                                                        <Col md={3} id="presentacion_info_producto"
                                                             style={{color: 'red', fontWeight: '600'}}
                                                             md={3}>${Intl.NumberFormat().format((infoProductos.Prod_Precio))} </Col>
                                                        <Col md={6} id="presentacion_info_producto">
                                                            <div> Acumulas</div>
                                                        </Col>
                                                        <Col md={6} id="presentacion_info_producto" style={{
                                                            fontSize: '14px',
                                                            textAlign: 'end',
                                                            alignSelf: 'center'
                                                        }}>340 RipleyPuntos GO </Col>

                                                        <Col md={12} style={{marginTop: '40px'}}>
                                                            <Button className="button_carrito"
                                                                    onClick={() => this.handleAgregarProd(infoProductos)}> Agregar
                                                                a la
                                                                bolsa</Button>
                                                        </Col>

                                                    </Row>

                                                </Col>
                                            </div>
                                        </Col>
                                        <Col md={12}>
                                            <div style={{marginTop: '50px'}}>
                                                <h4>DESCRIPCIÓN</h4>
                                            </div>
                                            <div className="style-descripcion_producto">
                                                <p id="presentacion_producto">{infoProductos.Prod_nombre} </p>
                                                <p>{infoProductos.Prod_descripcion}</p>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>}

                        </Col>
                    </div>
                </div>
            </div>
        );
    }
};


const initMapStateToProps = state => ({
        infoProductos: state.infoProductos,
        prod_carrito: state.prod_carrito,
        cant_compra: state.cant_compra
    })

;
const initMapDispatchToProps = dispatch => ({
        handleInfoProducto(infoProductos) {
            dispatch({
                type: "INFO_PRODUCTOS",
                infoProductos
            });
            // console.log(infoProductos);
        },
        handleProdBolsa(prod_carrito) {
            dispatch({
                type: "PROD_CARRITO",
                prod_carrito
            });

        },
        handleCantidadProductos(cantidad_carrito) {
            dispatch({
                type: "CANTIDAD_COMPRA",
                cantidad_carrito
            });
            console.log(cantidad_carrito);
        }

    })
;

export default connect(initMapStateToProps, initMapDispatchToProps)(Productos);
