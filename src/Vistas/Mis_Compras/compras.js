import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faDollarSign, faDolly, faHome, faMobileAlt, faUser} from "@fortawesome/free-solid-svg-icons";
import './style.css';
import {Button, Col, Row} from 'reactstrap';
import connect from "react-redux/es/connect/connect";
import axios from "axios";

import moment from "moment";


const carrito = <FontAwesomeIcon style={{color: '#70578b'}} icon={faDolly}/>;
const usuario = <FontAwesomeIcon style={{color: '#70578b'}} icon={faUser}/>;
const mobile = <FontAwesomeIcon style={{color: '#70578b'}} icon={faMobileAlt}/>;
const signo = <FontAwesomeIcon style={{color: '#70578b'}} icon={faDollarSign}/>;
const casa = <FontAwesomeIcon style={{color: '#70578b'}} icon={faHome}/>;


class Compras extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ordenCompra: this.props.ordenCompra,
            orden: ''
        };
    }

    handleCerrar = () => {
        let {hanfleformularioOrdenCompra, handleComprar, handleFormularioVenta, handleModal} = this.props;
        hanfleformularioOrdenCompra(false);
        handleModal(false);
        handleFormularioVenta(true);
        handleComprar(false);
    };

    componentDidMount() {
        // console.log(this.props.ordenCompra);
        axios.get(`${process.env.REACT_APP_API_URL}getOrdenCompra/${this.props.ordenCompra}`)
            .then(res => {
                let array = res.data.result;
                let nuevoArray = {};
                let precioFinal = 0;
                array.map((elemento, index) => {
                    precioFinal = precioFinal + parseInt(elemento.Precio_producto);
                    nuevoArray = {
                        Precio_total_producto: precioFinal,
                        direccion_cliente: elemento.direccion_cliente,
                        email_cliente: elemento.email_cliente,
                        nombre_cliente: elemento.nombre_cliente,
                        nro_orden_compra: elemento.nro_orden_compra,
                        telefono_cliente: elemento.telefono_cliente,
                        oc_fecha_emision: elemento.oc_fecha_emision
                    };
                    // console.log(nuevoArray);
                });
                this.setState({orden: nuevoArray});
            });
    }

    render() {
        let {orden} = this.state;
        return (
            <div className="contenedor-carrito">
                <div className="titulo-carrito">
                    <h5> {carrito} Orden de compra </h5>
                </div>
                <div className="prod-carrito-vacio">
                    <div style={{
                        padding: '30px', marginRight: '57px',
                        marginLeft: '57px'
                    }}>
                        <h4 style={{
                            color: '#4e2e7d',
                            marginBottom: '10px'
                        }}>
                            ¡Tu compra está confirmada!</h4>
                        <h6 style={{
                            color: '#4e2e7d',
                            marginBottom: '10px'
                        }}>
                            Revisa la boleta adjunta, no es necesario que la imprimas. Si la necesitas, muéstrala
                            directamente desde tu celular.</h6>

                        <div style={{marginLeft: '130px'}}>
                            <Row>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{usuario}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span> Nombre Comprador:</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {orden.nombre_cliente}
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{usuario}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span> Folio N°</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {orden.nro_orden_compra}
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{usuario}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span> Fecha de emisión:</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {/*{moment(orden.oc_fecha_emision).format('MMMM Do YYYY, h:mm:ss a')}*/}{moment(orden.oc_fecha_emision).format('DD-MM-YYYY HH:mm:ss')}

                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{mobile}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span> Teléfono:</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {orden.telefono_cliente}
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{signo}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span>Total Compra:</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {Intl.NumberFormat().format((orden.Precio_total_producto))}
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    <Row>
                                        <Col md={2} className="positionIcon">{casa}
                                        </Col>
                                        <Col className="marginIcon">
                                            <span>Direccion:</span>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col md={6} id="style-informacion-orden-compra">
                                    {orden.direccion_cliente}
                                </Col>
                            </Row>
                        </div>
                        <Col md={12} style={{marginTop: '70px'}}>
                            <Button size="sm" onClick={() => this.handleCerrar()}>Cerrar </Button>
                        </Col>
                    </div>
                </div>
            </div>
        );
    }
}

const initMapStateToProps = state => ({
        infoOrden: state.infoOrden,
        compraFinalizada: state.compraFinalizada,
        modalVisible: state.modalVisible,
        formularioOrdenCompra: state.formularioOrdenCompra,
        isFormVenta: state.isFormVenta,
        isComprar: state.isComprar
    })
;
const initMapDispatchToProps = dispatch => ({
    handleProdBolsa(prod_carrito) {
        dispatch({
            type: "PROD_CARRITO",
            prod_carrito
        });
    },
    handleCompraFinalizada(compraFinalizada) {
        dispatch({
            type: "COMPRA_FINALIZADA",
            compraFinalizada
        });

    },
    handleModal(modalVisible) {
        dispatch({
            type: "MODAL_ISVISIBLE",
            modalVisible
        });
    },
    hanfleformularioOrdenCompra(formularioOrdenCompra) {
        dispatch({
            type: "FORMULARIO_VENTA",
            formularioOrdenCompra
        });

    },
    handleFormularioVenta(isFormVenta) {
        dispatch({
            type: "VENTA_ISVISIBLE",
            isFormVenta
        });
        // console.log(isFormVenta);
    },
    handleComprar(isComprar) {
        dispatch({
            type: "COMPRAR_ISVISIBLE",
            isComprar
        });
        // console.log(isComprar);
    },
});

export default connect(initMapStateToProps, initMapDispatchToProps)(Compras);

