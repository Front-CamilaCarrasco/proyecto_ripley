import React, {Component} from 'react';
import {Button, Col, Form, Input, Row} from 'reactstrap';
import './style.css';
import connect from "react-redux/es/connect/connect";
import axios from "axios";
import Compras from "../Mis_Compras/compras";
import Rut from '../../Components/Rut/Rut';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingBag} from "@fortawesome/free-solid-svg-icons";

const bolsa = <FontAwesomeIcon style={{color: '#70578b'}} icon={faShoppingBag}/>;


class FormularioVenta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formularioOrdenCompra: false,
            totalCompra: props.totalCompra,
            ordenCompra: 0,
            validar: false,
            validarNombre: false,
            inputs: {
                nombres: '',
                apellidos: '',
                rut: '',
                correo: '',
                telefono: '',
                direccion: '',
                nro_tarjeta: ''
            }
        };
    }

    handleInputs(input, event) {
        let inputs = this.state.inputs;
        switch (input) {
            case 'nombres':
                inputs.nombres = event.target.value;
                break;
            case 'apellidos':
                inputs.apellidos = event.target.value;
                break;
            case 'telefono':
                inputs.telefono = event.target.value;
                break;
            case 'rut':
                inputs.rut = event.target.value;
                break;
            case 'correo':
                inputs.correo = event.target.value;
                break;
            case 'direccion':
                inputs.direccion = event.target.value;
                break;
            case 'nro_tarjeta':
                inputs.nro_tarjeta = event.target.value;
                break;
            default:
                break;
        }
        this.setState({inputs});
    }

    handleValidarFormulario = () => {
        let inputs = this.state.inputs;
        if (inputs['nombres'].length === 0 || inputs['nombres'].length == null || /^\s+$/.test(inputs['nombres'])) {
            alert("Debe ingresar alemenos un nombre");
            var nombre = document.getElementById("nombres");
            nombre.focus();
            return false;
        } else if (inputs['apellidos'].length === 0 || inputs['apellidos'].length == null || /^\s+$/.test(inputs['apellidos'])) {
            alert("Debe ingresar alemenos un apellido");
            var apellido = document.getElementById("apellidos");
            apellido.focus();
            return false;
        } else if (inputs['rut'].length === 0) {
            alert("Debe ingresar el rut");
            var rut = document.getElementById("rut");
            rut.focus();
            return false;
        } else if (inputs['correo'].length === 0 || (/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)/.test(inputs['correo']))) {
            alert("Debe ingresar un correo electronico");
            var correo = document.getElementById("correo");
            correo.focus();
            return false;
        } else if (inputs['telefono'].length === 0 || !(/^\d{9}$/.test(inputs['telefono']))) {
            alert("Debe ingresar alemenos el numero de teléfono");
            var telefono = document.getElementById("telefono");
            telefono.focus();
            return false;
        }
        else if (inputs['direccion'].length === 0) {
            alert("Debe ingresar alemenos el numero de dirección");
            var direccion = document.getElementById("direccion");
            direccion.focus();
            return false;
        } else if (inputs['nro_tarjeta'].length === 0 || isNaN(inputs['nro_tarjeta'])) {
            alert("Debe ingresar alemenos el numero de tarjeta");
            var nro_tarjeta = document.getElementById("nro_tarjeta");
            nro_tarjeta.focus();
            return false;
        } else {
            this.handleNuevoCliente();
        }
    };


    handleNuevoCliente = () => {
        let resId = 0;
        let odenCompraFinal = 0;
        let {prod_carrito} = this.props;
        let {
            nombres, apellidos, rut, correo, telefono, direccion,  nro_tarjeta
        } = this.state.inputs;

        axios.post(`${process.env.REACT_APP_API_URL}nuevoCliente`, {
                cli_nombre: nombres,
                cli_apellido: apellidos,
                cli_rut: rut,
                cli_email: correo,
                cli_telefono: telefono,
                cli_direccion: direccion,
                cli_nro_tarjeta: nro_tarjeta
            }
        ).then((res) => {
            resId = res.data.results.insertId;
            // console.log(resId);
            prod_carrito.map(element => {
                // console.log(res.data.results.insertId);
                axios.post(`${process.env.REACT_APP_API_URL}nuevaVenta`, {
                    v_cli_id: res.data.results.insertId,
                    v_prod_id: element.Prod_id,
                    od_fecha_emision:new Date()
                });
            });

        }).then(() => {
            odenCompraFinal = (123 * resId);
            axios.post(`${process.env.REACT_APP_API_URL}nuevaOrdenCompra`, {
                od_v_id: resId,
                oc_codigo: odenCompraFinal
            });
        }).then(() => {
            this.setState({
                ordenCompra: odenCompraFinal
            });
            // console.log(this.state.ordenCompra);
            let {handleLimpiarCarrito, hanfleformularioOrdenCompra} = this.props;
            handleLimpiarCarrito(prod_carrito);
            hanfleformularioOrdenCompra(true);


        });
    };

    render() {
        let {
            nombres, apellidos, rut, correo, telefono, direccion, modoPago, nro_tarjeta
        }

            = this.state.inputs;
        let {handleComprar, formularioOrdenCompra, prod_carrito} = this.props;
        return (
            <div>
                {formularioOrdenCompra ?
                    <Compras ordenCompra={this.state.ordenCompra}/> :
                    <>
                        <div className="titulo-carrito">
                            <h5> {bolsa} Bolsa de compra </h5>
                        </div>
                        <div className="contenedor-carrito">
                            <div className="contenedor-form-venta">
                                <Row>
                                    <Col md={6}>
                                        <div>
                                            <Row style={{textAlign: '-webkit-center'}}>

                                                <div id="overflow-carrito">
                                                    {prod_carrito.map((element, index) =>
                                                            <div style={{
                                                                display: 'flex',
                                                                flexWrap: 'wrap',
                                                                marginleft: '-15px',
                                                            }} key={index}>
                                                                <Col md={3} className="img-formulario-venta">
                                                                    <img width="100%" src={element.Prod_img}/>
                                                                </Col>
                                                                <Col md={9}>
                                                                        <span className="style-nombre"
                                                                              id="presentacion_producto">{element.Prod_nombre}</span><br></br>
                                                                    <span className="style-nombre"
                                                                          id="presentacion_codigo_producto">Codigo: {element.Prod_codigo}</span><br></br>
                                                                    <span id="presentacion_info_producto">
                                                                    <span
                                                                        style={{fontSize: '12px'}}> Precio Unidad ${Intl.NumberFormat().format((element.Prod_Precio))} </span>
                                            </span>
                                                                </Col>
                                                                <hr id="hr-style-producto-lista"></hr>
                                                            </div>
                                                    )}
                                                </div>
                                                <Col md={12} style={{marginTop: '24px'}}>
                                                    <Col md={12}>
                                                        <div style={{fontSize: '15px'}}>Total Compra:</div>
                                                    </Col>
                                                    <Col md={12}>
                                                        <div style={{
                                                            fontSize: '15px',
                                                            fontWeight: '700'
                                                        }}>${Intl.NumberFormat().format((this.state.totalCompra))}</div>
                                                    </Col>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                    <Col md={6}>
                                        <Form>
                                            <Row>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label for="nombres"
                                                               style={{width: '100%'}}>NOMBRES:</label>
                                                    </div>
                                                    <Input
                                                        class="form-control"
                                                        bsSize="sm"
                                                        type="text"
                                                        id="nombres"
                                                        placeholder="Luis Antonio"
                                                        value={nombres}
                                                        onChange={e => this.handleInputs('nombres', e)}
                                                    />
                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label for="apellidos"
                                                               style={{width: '100%'}}>APELLIDOS:</label>
                                                    </div>
                                                    <Input
                                                        bsSize="sm"
                                                        type="text"
                                                        placeholder="Maulen Carrasco"
                                                        id="apellidos"
                                                        value={apellidos}
                                                        onChange={e => this.handleInputs('apellidos', e)}
                                                    />
                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label style={{width: '100%'}}>RUT:</label>
                                                    </div>
                                                    <Rut value={rut} onChange={e => this.handleInputs('rut', e)}>
                                                        <Input type="text" name="rut"
                                                               style={{fontSize: '13px'}}
                                                               id="rut"
                                                               value={rut}
                                                               placeholder="12.456.345-k"
                                                        />
                                                    </Rut>

                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label style={{width: '100%'}}>CORREO ELECTRONICO:</label>
                                                    </div>
                                                    <Input
                                                        bsSize="sm"
                                                        type="text"
                                                        id="correo"
                                                        placeholder="exameple@example.com"
                                                        value={correo}
                                                        onChange={e => this.handleInputs('correo', e)}
                                                    />
                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label style={{width: '100%'}}>TELÉFONO:</label>
                                                    </div>
                                                    <Input
                                                        bsSize="sm"
                                                        type="text"
                                                        id="telefono"
                                                        placeholder="968781957 / 227711682"
                                                        value={telefono}
                                                        onChange={e => this.handleInputs('telefono', e)}
                                                    />
                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label style={{width: '100%'}}>DIRECCIÓN:</label>
                                                    </div>
                                                    <Input
                                                        bsSize="sm"
                                                        type="text"
                                                        id="direccion"
                                                        placeholder="San juan #433"
                                                        value={direccion}
                                                        onChange={e => this.handleInputs('direccion', e)}
                                                    />
                                                </Col>
                                                <Col md="12" id="cont-input-label">
                                                    <div id="label-input-form">
                                                        <label style={{width: '100%'}}>N° TARJETA:</label>
                                                    </div>
                                                    <Input
                                                        bsSize="sm"
                                                        type="text"
                                                        id="nro_tarjeta"
                                                        placeholder="23452345654356432"
                                                        value={nro_tarjeta}
                                                        onChange={e => this.handleInputs('nro_tarjeta', e)}
                                                    />
                                                </Col>
                                            </Row>
                                        </Form>
                                    </Col>
                                    <Col md={12}>
                                        <div style={{
                                            marginTop: '12px',
                                            marginLeft: '200px',
                                            marginRight: '200px',
                                            textAlign:'center'
                                        }}>
                                            <Row>
                                                <Col md={6}>
                                                    <Button size="sm" className="button_carrito"
                                                            onClick={() => this.handleValidarFormulario()}>Pagar</Button>
                                                </Col>
                                                <Col md={6}>
                                                    <Button size="sm"
                                                            className="button_carrito"
                                                            onClick={() => handleComprar(false)}>Atras</Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                </Row>
                            </div>

                        </div>
                    </>}
            </div>
        );
    }
}

const initMapStateToProps = state => ({
        prod_carrito: state.prod_carrito,
        isComprar: state.isComprar,
        formularioOrdenCompra: state.formularioOrdenCompra
    })

;
const initMapDispatchToProps = dispatch => ({
        handleLimpiarCarrito(prod_carrito) {
            dispatch({
                type: "LIMPIAR_CARRITO",
                prod_carrito
            });
        },
        handleFormularioVenta(isFormVenta) {
            dispatch({
                type: "VENTA_ISVISIBLE",
                isFormVenta
            });
            // console.log(isFormVenta);
        },
        handleComprar(isComprar) {
            dispatch({
                type: "COMPRAR_ISVISIBLE",
                isComprar
            });
            // console.log(isComprar);
        },
        hanfleformularioOrdenCompra(formularioOrdenCompra) {
            dispatch({
                type: "FORMULARIO_VENTA",
                formularioOrdenCompra
            });
        },

    })
;

export default connect(initMapStateToProps, initMapDispatchToProps)(FormularioVenta);

