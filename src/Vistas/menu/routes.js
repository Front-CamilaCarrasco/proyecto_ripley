import React from "react";
import Productos from "../Productos/productos";
import Home from "../Home/Home";

 const routes = [
    {
        path: "/",
        exact: true,
        sidebar: () => <Home />,
        main: () => ''
    },
    {
        path: "/Perfumeria",
        sidebar: () => <Productos/>,
        main: () => <Productos/>
    },
    {
        path: "/shoelaces",
        sidebar: () => <div>shoelaces!</div>,
        main: () => <h2>Shoelaces</h2>
    }
];

export default routes

