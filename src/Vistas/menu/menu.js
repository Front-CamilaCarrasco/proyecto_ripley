import React, {useState} from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import {Col, Collapse, Navbar, NavbarToggler, Row} from 'reactstrap';
import routes from './routes';
import './style.css';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faBaby, faHome, faShoePrints, faSprayCan, faTv} from '@fortawesome/free-solid-svg-icons';
import Modals from "../../Vistas/Carro_ventas/Modal/modal";

const tecno = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faTv}/>;
const home = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faHome}/>;
const baby = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faBaby}/>;
const zapatos = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faShoePrints}/>;
const perfume = <FontAwesomeIcon style={{marginRight: '6px'}} icon={faSprayCan}/>;

export default function SidebarExample() {

    const [collapsed, setCollapsed] = useState(true);
    const [abrirCarrito, setAbrirCarrito] = useState(true);
    const toggleNavbar = () => setCollapsed(!collapsed);


    return (
        <Router>

            <div
                style={{
                    width: "100%",
                    background: "#1a1a1a"
                }}
            >
                <Navbar color="faded" light>
                    <Col md={2} xs={4} style={{display: 'flex', placeSelf: 'flex-start'}}>
                        <NavbarToggler onClick={toggleNavbar} className="mr-2"/>
                        <div onClick={toggleNavbar} style={{alignSelf: 'center', cursor: 'pointer'}}>
                            <div className="menu-letras"> Menú de Categorias</div>
                        </div>
                    </Col>
                    <Col md={7} xs={3} style={{placeSelf: 'flex-start'}}>
                        {collapsed ?
                            <input type="search" placeholder="¿Qué estás buscando?" className="search-bar" value=""/>
                            : <Collapse isOpen={!collapsed} navbar>
                                <div
                                    style={{
                                        padding: "10px",
                                        width: "100%",
                                    }}
                                >
                                    <Row >
                                        <Col md={3} xs={6}>
                                            <ul>
                                                <Link to="/"> {tecno}Electro</Link>
                                            </ul>
                                        </Col>
                                        <Col md={3} xs={6}>
                                            <ul>
                                                <Link to="/">{home} Hogar</Link>
                                            </ul>
                                        </Col>
                                        <Col md={3} xs={6}>
                                            <ul>
                                                <Link to="/">{baby}Infantil</Link>
                                            </ul>
                                        </Col>
                                        <Col md={3} xs={6}>
                                            <ul>
                                                <Link to="/">{zapatos}Zapatos</Link>
                                            </ul>
                                        </Col>
                                        <Col md={3} xs={6}>
                                            <ul>
                                                <Link to="/Perfumeria">{perfume}Perfumeria</Link>
                                            </ul>
                                        </Col>
                                    </Row>
                                </div>
                            </Collapse>}

                    </Col>
                    <Col md={3} xs={5} style={{color: '#ffff', textAlign: 'center', placeSelf: 'flex-start', display: 'flex'}}>
                        <Col md="6" xs={7}>
                            <div style={{fontSize: '12px', cursor: 'pointer', marginTop: '3px'}}>
                                <span>Bienvenid@</span><br></br>
                                <span>Iniciar Sesión</span>
                            </div>
                        </Col>
                        <Col xs={6}>
                            <div>
                                <Modals/>
                            </div>
                        </Col>
                    </Col>


                </Navbar>

                <Switch>
                    {routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            exact={route.exact}
                            children={<route.sidebar/>
                            }
                        />
                    ))

                    }

                </Switch>
            </div>


        </Router>


    );


}




